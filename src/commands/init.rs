use super::commit::Commit;

use std::{fs, path::PathBuf};
pub fn Init(path: PathBuf) {
    let path_string = path.to_str().unwrap();
    let err = fs::create_dir_all(format!("{path_string}/vcs/objects"));
    fs::create_dir_all(format!("{path_string}/vcs/refs"));
    fs::File::create(format!("{path_string}/vcs/HEAD"));
    fs::write(
        format!("{path_string}/vcs/HEAD"),
        format!("{path_string}/vcs/refs/main"),
    );
    Commit("Initial commit".to_string());
}
