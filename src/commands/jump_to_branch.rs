use crate::repo_file_manager::GetProjectDirectory;
use std::fs;

use super::jump_to_commit::JumpToCommit;
use std::str::from_utf8;

pub fn JumpToBranch(branch_name: String) {
    let mut refs_dir = GetProjectDirectory("./".to_string());
    let res = fs::write(
        format!("{refs_dir}/vcs/HEAD"),
        format!("{refs_dir}/vcs/refs/{branch_name}"),
    );
    refs_dir.push_str("/vcs/refs");
    let hash = fs::read(format!("{refs_dir}/{branch_name}")).unwrap();
    let hash = from_utf8(&hash).unwrap();
    JumpToCommit(hash.to_string());
}
