use crate::repo_file_manager::GetProjectDirectory;

use std::fs;

pub fn NewBranch(branch_name: String) {
    if (CheckBranchName(branch_name.clone())) {
        let mut head_dir = GetProjectDirectory("./".to_string());
        let root = head_dir.clone();
        head_dir.push_str("/vcs/HEAD");
        fs::write(head_dir, format!("{root}/vcs/refs/{branch_name}"));
    } else {
        println!("{}", "error");
    }
}

pub fn CheckBranchName(branch_name: String) -> bool {
    let mut refs_dir = GetProjectDirectory("./".to_string());
    refs_dir.push_str("/vcs/refs");
    let dir = fs::read_dir(refs_dir).unwrap();
    for path in dir {
        let filename = path.unwrap().path();
        if filename.ends_with(branch_name.clone()) {
            return false;
        }
    }
    return true;
}
