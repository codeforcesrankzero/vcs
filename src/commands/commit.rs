use crate::repo_file_manager::{GetProjectDirectory, GetTree, MakeCommitObj, MakeRef};

pub fn Commit(message: String) {
    let mut vcs_directory = GetProjectDirectory("./".to_string());
    vcs_directory.push_str("/vcs");
    MakeRef(MakeCommitObj(GetTree().unwrap(), message));
}
