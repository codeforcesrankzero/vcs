use crate::repo_file_manager::GetProjectDirectory;
use miniz_oxide::{
    deflate::{self, stream::deflate},
    inflate,
};
use sha1::{Digest, Sha1};
use std::fs::DirEntry;
use std::{env, fmt::format, fs, hash, mem::needs_drop, path::PathBuf};

use std::str::from_utf8;

use std::collections::HashMap;

pub fn CheckFiles(files_dir: PathBuf, map_of_codes: &HashMap<String, String>) {
    let directory = fs::read_dir(files_dir.clone()).unwrap();
    for file in directory {
        let p_name = file.unwrap().path();
        let last_file = p_name.iter().last().unwrap().to_str();
        if p_name.is_file() {
            let mut hasher = Sha1::new();
            let blob_content = deflate::compress_to_vec_zlib(&fs::read(p_name.clone()).unwrap(), 8);
            let mut blob_content = hex::encode(blob_content);
            if (map_of_codes.contains_key(last_file.unwrap())) {
                if map_of_codes[last_file.unwrap()] != blob_content {
                    println!("modified: {:?}", p_name);
                }
            } else {
                println!("added: {:?}", p_name);
            }
        } else {
            if !p_name.ends_with(".git")
                && !p_name.ends_with("target")
                && !p_name.ends_with(".vscode")
                && !p_name.ends_with("vcs")
            {
                CheckFiles(p_name, map_of_codes);
            }
        }
    }
}

pub fn Status() {
    let mut vcs_dir = GetProjectDirectory("./".to_string());
    vcs_dir.push_str("/vcs");
    let repo_dir = vcs_dir[..(vcs_dir.len() - 3)].to_string();
    let hash_of_files = GetMapOfFiles(vcs_dir.clone());
    let current_branch = (&fs::read(format!("{vcs_dir}/HEAD")).unwrap());
    let current_branch = from_utf8(current_branch).unwrap().split("/");
    let current_branch: Vec<&str> = current_branch.collect();
    println!("On branch {}", current_branch.last().unwrap());
    CheckFiles(repo_dir.try_into().unwrap(), &hash_of_files);
}

pub fn AddObjToMap<'a>(
    hashmap: &'a mut HashMap<String, String>,
    object_dir: PathBuf,
) -> &'a mut HashMap<String, String> {
    let directory = fs::read_dir(object_dir.clone()).unwrap();
    for object in directory {
        let readed = from_utf8(&fs::read(object.unwrap().path()).unwrap())
            .unwrap()
            .to_string();
        let splitted_blob = readed.split(" ");
        let splitted_blob: Vec<&str> = splitted_blob.collect();
        println!("{}", readed);
        if (splitted_blob.len() > 2) {
            if splitted_blob[1] == "blob" {
                // println!("{:?}", splitted_blob[2]);
                hashmap.insert(splitted_blob[2].to_string(), splitted_blob[0].to_string());
            }
        }
    }
    hashmap
}

pub fn GetMapOfFiles(mut vcs_dir: String) -> HashMap<String, String> {
    vcs_dir.push_str("/objects");
    let mut objects_hashmap: HashMap<String, String> = HashMap::new();
    let objects_vcs = fs::read_dir(vcs_dir.clone()).unwrap();
    for object in objects_vcs {
        let path_to_object = object.as_ref().unwrap().path();
        let object_name = path_to_object.iter().last().unwrap().to_str();
        AddObjToMap(&mut objects_hashmap, object.unwrap().path());
    }
    // for element in &objects_hashmap{
    //     println!("{:?}", element.0);
    // }
    objects_hashmap
}
