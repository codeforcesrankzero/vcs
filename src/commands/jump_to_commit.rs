use crate::repo_file_manager::{DeleteFullRepo, GetProjectDirectory, WriteFilesFromTree};

use std::fs;

use std::str::from_utf8;

pub fn JumpToCommit(commit_hash: String) {
    let dir = commit_hash[..2].to_string();
    let name = commit_hash[2..].to_string();
    let main_dir = GetProjectDirectory("./".to_string());
    let mut vcs_dir = GetProjectDirectory("./".to_string());
    vcs_dir.push_str("/vcs");
    let commit_obj = fs::read(format!("{vcs_dir}/objects/{dir}/{name}")).unwrap();
    let commit_obj = from_utf8(&commit_obj).unwrap();
    let splitted_commit_object: Vec<&str> = commit_obj.split("\n").collect();
    DeleteFullRepo(format!("{main_dir}").try_into().unwrap());
    WriteFilesFromTree(splitted_commit_object[0].to_string(), vcs_dir, main_dir);
}
