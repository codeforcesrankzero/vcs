use miniz_oxide::{deflate, inflate};
use sha1::{Digest, Sha1};
use std::{fs, path::PathBuf, str::from_utf8};

use hex;
pub fn GetProjectDirectory(path: String) -> String {
    let directory = fs::read_dir(path.clone()).unwrap();
    let mut information_for_tree: Vec<String> = vec![];
    for path in directory {
        let mut p_name = path.unwrap().path();
        let last_file = p_name.iter().last().unwrap().to_str().unwrap();
        if p_name.ends_with("vcs") {
            p_name.pop();
            return p_name.to_str().unwrap().to_string();
        }
        if p_name.is_dir() {
            let tmp = GetProjectDirectory(p_name.to_str().unwrap().to_string());
            if (tmp.len() > 0) {
                return tmp;
            }
        }
    }
    return "".to_string();
}

pub fn WriteBlob(file_to_write: PathBuf, vcs_dir: String) -> String {
    let mut hasher = Sha1::new();
    let blob_content = deflate::compress_to_vec_zlib(&fs::read(file_to_write.clone()).unwrap(), 8);
    let mut blob_content = hex::encode(blob_content);
    let filename = file_to_write.iter().last().unwrap().to_str().unwrap();
    blob_content.push_str(format!(" blob {filename}").as_str());
    hasher.update(blob_content.clone());
    let finalize = hasher.finalize();
    let hexed_hash = hex::encode(finalize);
    let dir = &hexed_hash[..2];
    let file = &hexed_hash[2..];
    fs::create_dir_all(format!("{vcs_dir}/objects/{dir}"));
    fs::write(format!("{vcs_dir}/objects/{dir}/{file}"), blob_content).unwrap();
    hexed_hash
}

pub fn CheckDirBanned(path: PathBuf) -> bool {
    let mut vcs_dir = GetProjectDirectory("./".to_string());
    vcs_dir.push_str("/vcs");
    let banned = [".git", "target", "target", "vcs", "vscode"];
    for ban in banned {
        if path.ends_with(ban) {
            return true;
        }
    }
    return false;
}

pub fn CodeTree(information_for_tree: Vec<String>) -> String {
    let mut tree_content: Vec<u8> = vec![];
    for blob in &information_for_tree {
        for byte in deflate::compress_to_vec_zlib(blob.as_bytes(), 8) {
            tree_content.push(byte);
        }
    }
    let mut hasher = Sha1::new();
    hasher.update(tree_content.clone());
    let res = hex::encode(hasher.finalize());
    res
}

pub fn GetStringTreeRepr(information_for_tree: Vec<String>) -> String {
    let mut stacked_file_input: String = "".to_string();
    for blob in &information_for_tree {
        stacked_file_input.push_str(blob);
        stacked_file_input.push('\n');
    }
    stacked_file_input
}

pub fn GetTree() -> Option<String> {
    let path = GetProjectDirectory("./".to_string());
    let project_directory = GetProjectDirectory("./".to_string());
    let mut vcs_dir = project_directory.clone();
    vcs_dir.push_str("/vcs");
    let directory = fs::read_dir(path.clone()).unwrap();
    let mut information_for_tree: Vec<String> = vec![];
    for path in directory {
        let p_name = path.unwrap().path();
        let last_file = p_name.iter().last().unwrap().to_str().unwrap();
        if p_name.is_file() {
            let hexed_hash = WriteBlob(p_name.clone(), vcs_dir.clone());
            information_for_tree.push(format!("blob {hexed_hash} {last_file}"));
        }

        if p_name.is_dir() && !CheckDirBanned(p_name.clone()) {
            let sub_tree = GetTree();
            match sub_tree {
                Some(val) => {
                    let to_push = format!("tree {val} {last_file}");
                    information_for_tree.push(to_push);
                }
                None => {}
            }
        }
    }
    let coded_tree = CodeTree(information_for_tree.clone());
    if !(coded_tree.len() == 0) {
        let dir = &coded_tree[..2];
        let file = &coded_tree[2..];
        fs::create_dir_all(format!("{vcs_dir}/objects/{dir}"));
        fs::write(
            format!("{vcs_dir}/objects/{dir}/{file}"),
            GetStringTreeRepr(information_for_tree),
        );
        return Some(coded_tree);
    }

    None
}

pub fn MakeRef(commit_object: String) {
    let mut vcs_dir = GetProjectDirectory("./".to_string());
    vcs_dir.push_str("/vcs");
    let head_content = fs::read(format!("{vcs_dir}/HEAD")).unwrap();
    let head_content = from_utf8(&head_content).unwrap();
    let mut path_to_ref = PathBuf::from(head_content);
    path_to_ref.pop();
    fs::create_dir_all(path_to_ref);
    fs::write(head_content, commit_object);
}

pub fn MakeCommitObj(tree: String, commit_msg: String) -> String {
    let mut vcs_dir = GetProjectDirectory("./".to_string());
    vcs_dir.push_str("/vcs");
    let mut stacked_commit: String = "".to_string();
    stacked_commit.push_str(tree.as_str());
    stacked_commit.push_str("\n");
    stacked_commit.push_str(&commit_msg.as_str());
    let mut hasher = Sha1::new();
    hasher.update(stacked_commit.clone());
    let hexed_commit = hex::encode(hasher.finalize());
    let directory: String = hexed_commit[..2].to_string();
    let name: String = hexed_commit[2..].to_string();
    let path_to_commit = format!("{vcs_dir}/objects/{directory}/{name}");
    fs::create_dir_all(format!("{vcs_dir}/objects/{directory}"));
    fs::File::create(path_to_commit.clone());
    fs::write(path_to_commit, stacked_commit);
    return hexed_commit;
}
pub fn GetBlobFromHash(hash: String, vcs_dir: String) -> String {
    let dir = hash[..2].to_string();
    let name = hash[2..].to_string();
    let readed = fs::read(format!("{vcs_dir}/objects/{dir}/{name}")).unwrap();
    let readed_string: Vec<&str> = from_utf8(&readed).unwrap().split(" ").collect();
    return readed_string[0].to_string();
}
pub fn WriteFilesFromTree(tree_hash: String, vcs_dir: String, current_dir: String) {
    let dir = tree_hash[..2].to_string();
    let name = tree_hash[2..].to_string();
    let obejct_content = fs::read(format!("{vcs_dir}/objects/{dir}/{name}")).unwrap();
    let obejct_content = from_utf8(&obejct_content).unwrap().to_string();
    let splitted_content: Vec<&str> = obejct_content.split("\n").collect();
    for line in splitted_content {
        let splitted_line: Vec<&str> = line.split(" ").collect();
        if splitted_line.len() > 2 {
            if (splitted_line[0] == "blob") {
                let mut filename = splitted_line[2].to_string();
                let mut splitted_file: Vec<&str> = filename.split(".").collect();
                let mut filename = splitted_file[0].to_string();
                if (splitted_file.len() > 1) {
                    filename.push_str(".");
                    filename.push_str(splitted_file[1]);
                }
                fs::File::create(format!("{current_dir}/{filename}"));
                let coded_blob = GetBlobFromHash(splitted_line[1].to_string(), vcs_dir.clone());
                fs::write(
                    format!("{current_dir}/{filename}"),
                    GetFileFromCode(coded_blob),
                );
            } else {
                let mut deeper_current_dir = current_dir.clone();
                deeper_current_dir.push_str(splitted_line[2]);
                // deeper_current_dir.push_str(" aboba");
                fs::create_dir_all(deeper_current_dir.clone());
                WriteFilesFromTree(
                    splitted_line[1].to_string(),
                    vcs_dir.clone(),
                    deeper_current_dir.clone(),
                )
            }
        }
    }
}

pub fn DeleteFullRepo(current_dir: PathBuf) {
    let directory = fs::read_dir(current_dir.clone()).unwrap();
    let mut tmp_tree: Vec<String> = vec![];
    for path in directory {
        let p_name = path.unwrap().path();
        if !p_name.ends_with(".git")
            && !p_name.ends_with("target")
            && !p_name.ends_with(".vscode")
            && !p_name.ends_with("vcs")
        {
            fs::remove_dir_all(p_name);
            // println!("{}", p_name.to_str().unwrap());
        }
    }
}

pub fn GetFileFromCode(coded: String) -> String {
    let splitted: Vec<&str> = coded.split(" ").collect();
    let code = splitted[0];
    let code = hex::decode(code).unwrap();

    let uncoded = inflate::decompress_to_vec_zlib(&code).unwrap();
    let uncoded = from_utf8(&uncoded).unwrap();
    println!("{}", uncoded);
    uncoded.to_string()
}
